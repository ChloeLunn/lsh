# Limited-integrated SHell - LSH

## Info

lsh is a simple implementation of a shell in C, with the core operation based
on that from the original UNIX V6 shell, with many extensions and modifications.

It is not suitable to replace bash, zsh, ksh, ash, or whatever on any actual
installation, but may be fun to play with and expand.

The shell script parsing is actually a direct copy of a port of the original
unix Thompson Shell (the OG 'sh'), so may not be as complete as modern shells.
As is the globbing.

That means this project includes some code of Unix V6 from 1975... cool, right?
(Note that the code from UNIX V6 is licensed under the caldera license.)

Everything else was written from scratch by me, or uses tools from standard
libraries such as `unistd.h` and `string.h`.

Builtins include: `history`, `cd`, `pwd`, `exit`, `clear`, etc. See builtins.h
for more information.

Plan is to eventually mod it to conform to POSIX
[sh standard](https://pubs.opengroup.org/onlinepubs/009604599/utilities/sh.html)
and/or some of the extensions that are commonly required.

There will, however, be a build flag to disable any extensions or POSIX options
or arguments which are more intensive.

## Running

This project doesn't run! What... you want code that _runs_ now?

This project was originally written for Unish, and has now been pulled out.
That means, it can actually be copied to an arduino sketch folder!
As long as you then implement some of the system functions, or even just if you
have them do nothing, then you should be able to use the function from that
sketch.

I've added a handy `#ifdef` around the main function and some of the other
parts so that if you do use it in that way, it's fairly easy to copy it in with
minimal changes.

## Contributing

I have no clue how to actually use git stuff... so old school, if you have any
fixes or suggestions can you shoot me an email/message with it, please?

**Note:** There is [another project](https://github.com/brenns10/lsh) named
"lsh", which is also a simple shell, written by Stephen Brennan. But *this*
project was neither based on, derived from, nor inspired by it, despite any
similarities. Any resemblence is purely coincidental.

## License

This code is published under the MIT license, which means you can use, modify,
and distribute it without any restriction. I appreciate, but don't require,
acknowledgement in derivative works.
