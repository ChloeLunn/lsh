#include "sys_ext.h"
#include "version.h"

#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <pwd.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define SIMPLE_PROMPT (false)  // don't do full string, just do the #/% based on euid

// SECURE MODE:
// Secure mode is basically the same as normal mode, except every 5 commands it locks
//   the terminal down and requests a passphrase to allow you to use it again
//   there's a bit of a bug with pressing CTRL D so there are two options:
//   exit lsh completely on CTRL-D, or endless loop
#define SECURE_PROMPT         (false)        // regardless of euid, lock the terminal
#define SECURE_PHRASE         "chloe rulez"  // the lock phrase to use in secure mode
#define EXIT_SECURE_ON_CTRL_D (true)         // if the user pressed CTRL while locked, exit the shell (calls exit(-1))
#define SECURE_TTL            (5)            // time to live (how many commands between lockouts)
FILE* SECURE_IN;                             // input stream to use for secure prompt
FILE* SECURE_OUT;                            // output stream to use for secure prompt

// Shell value stuff
#define STDIN  (0)  // stdin
#define STDOUT (1)  // stdout
#define STDERR (2)  // stderr

#define ENOMEM  (12)
#define ENOEXEC (8)
#define E2BIG   (7)
#define ENOEXEC (8)
#define ENOENT  (2)

#define NAMESIZ   (64)
#define DIRSIZ    (512)
#define PROMPSIZ  (64)
#define HISTSIZ   (25)
#define STRSIZ    (512)
#define LINSIZ    (256)
#define TOKSIZ    (50)
#define TRESIZ    (100)
#define COMMANSIZ (LINSIZ)

#define QUOTE (0200)

// Attribute
#define FAND (1)
#define FCAT (2)
#define FPIN (4)
#define FPOU (8)
#define FPAR (16)
#define FINT (32)
#define FPRS (64)

// Type
#define TCOM (1)
#define TPAR (2)
#define TFIL (3)
#define TLST (4)

// Field
#define DTYP (0)
#define DLEF (1)
#define DRIT (2)
#define DFLG (3)
#define DSPR (4)
#define DCOM (5)

char ab[STRSIZ];
char* ava[50];
char** av;
char* string;
int errno;
int ncoll;

char* current_host_name = (char*)NULL;                   // current hostname
char* current_directory = (char*)NULL;                   // current directory
struct passwd* current_user = (struct passwd*)NULL;      // = getpwuid_r(getuid())
#define current_user_password (current_user->pw_passwd)  // password.... should give rubbish...
#define current_user_home_dir (current_user->pw_dir)     // home directory
#define current_user_name     (current_user->pw_name)    // user name
#define current_user_id       (current_user->pw_uid)     // user ID

// '$' indicator (for parsing args in scripts)
char* dolp;
char** dolv;
int dolc;

char pidp[4];

char* prompt = (char*)NULL;  // the string to print as the prompt call lsh_prompt_const char* to update

char* PS1;   // variable for storing the prompt format
char* TERM;  // variable for storing the type of terminal commands we should receive

#define default_term    ("VT52")         // this does nothing atm
#define default_prompt  ("[\\u: \\d]%")  // this is the default, but it can be changed (kinda...)
#define cwd_truncate_to (3)              // How many directories to truncate the current working directory to when printing it.

/* Prompt string options:
 * ----------------------
 * \u = username
 * \i = user id number
 * \h = hostname
 * \! = number in history
 * \^ = previous command
 * \d = current directory truncated to 3 levels with ~ sub for home by itself
 * \D = full current directory
 * `` = insert the std output from the command inside ``
 *  % = dynamically add % or # based on user
 *  # = as %
 */

// Line buffer
char line[LINSIZ];
char* linep;
char* elinep;

// Token list
char* toks[TOKSIZ];
char** tokp;
char** etokp;

// Syntax tree
unsigned long trebuf[TRESIZ];
unsigned long* treep;
unsigned long* treeend;

char peekc;

char overflow;
char glob;
char error;
char uid;
char setintr;

char* arginp;
int onelflg;

volatile bool freshPromptNeeded = true;

// for the lsh_read_line function
char c;
int len;
char* buf;
int strIdx;

char* mesg[] = {
  0,
  "Hangup",
  0,
  "Quit",
  "Illegal instruction",
  "Trace/BPT trap",
  "IOT trap",
  "EMT trap",
  "Floating exception",
  "Killed",
  "Bus error",
  "Memory fault",
  "Bad system call",
  0,
  "Sig 14",
  "Sig 15",
  "Sig 16",
  "Sig 17",
  "Sig 18",
  "Sig 19",
};

struct stime {
    int proct[2];
    int cputim[2];
    int systim[2];
} timeb;

// for history
int _cmdIdx = EOF;
unsigned int total_history = 0;
int hist_index = 0;
char line_history[HISTSIZ][COMMANSIZ];

#include "glob.c.h"

int need_full_secure_text = SECURE_TTL;

// works out the prompt to print, and updates the global prompt to match
void lsh_prompt_string(bool fresh, char* format)
{
#if SECURE_PROMPT
    if (need_full_secure_text >= SECURE_TTL)
    {
        while (1)
        {
            if (SECURE_OUT != NULL)
                fprintf(SECURE_OUT, "%s", "Prompt locked, type passphrase to continue > ");

            char passphrase[LINSIZ];
            memset(passphrase, 0x00, LINSIZ);

            char* t = NULL;

            if (SECURE_IN != NULL)
                t = fgets(passphrase, LINSIZ, SECURE_IN);

            if (t != NULL)
            {
                passphrase[strcspn(passphrase, "\r\n\x03")] = 0;
                passphrase[strcspn(passphrase, "\r\n\x03")] = 0;

                if (strcmp(passphrase, SECURE_PHRASE) != 0)
                {
                    if (SECURE_OUT != NULL)
                        fprintf(SECURE_OUT, "Incorrect passphrase. \r\n");
                }
                else
                {
                    if (SECURE_OUT != NULL)
                        fprintf(SECURE_OUT, "Prompt unlocked for next %i commands.\r\n", SECURE_TTL);
                    need_full_secure_text = 0;
                    break;
                }
            }
#if EXIT_SECURE_ON_CTRL_D
            else
            {
                if (SECURE_OUT != NULL)
                    fprintf(SECURE_OUT, "\r\n");
                exit(-1);
            }
#else
            SECURE_IN = NULL;
            SECURE_OUT = NULL;
#endif
        }
    }
    need_full_secure_text++;
#endif

    // Don't bother working on it if it's not needed
    if (!fresh)
        return;

#if SIMPLE_PROMPT
    // simple prompt, just do the character
    prompt = "% ";
    if (current_user_id == 0)
        prompt = "# ";
    return;
#else
    // user@SAMD:~%
    // user@SAMD:/dev/%

    // root@SAMD:~#
    // root@SAMD:/dev/#

    // grab current operating info
    current_user = getpwuid(geteuid());
    gethostname(current_host_name, NAMESIZ);
    getcwd(current_directory, DIRSIZ);

    char* workBuf = prompt;  // malloc(sizeof(char) * 1);
    workBuf[0] = 0;

    // parse the format variable for the formating of the string
    for (int i = 0; i < strlen(format); i++)
    {
        char c = format[i];

        // add a status variable to the string
        if (c == '\\')
        {
            c = format[++i];
            switch (c)
            {
            case 'e':  // escape
                strcat(workBuf, "\e");
                break;
            case 't':  // tab
                strcat(workBuf, "\t");
                break;
            case 'r':  // return
                strcat(workBuf, "\r");
                break;
            case 'n':  // newline
                strcat(workBuf, "\n");
                break;
            case 'h':  // host name
                strcat(workBuf, current_host_name);
                break;
            case 'u':  // user name
                strcat(workBuf, current_user_name);
                break;
            case 'i':  // user id
                {
                    char* idstr = newstr();
                    sprintf(idstr, "%i", current_user_id);
                    strcat(workBuf, idstr);
                    break;
                }
            case '!':  // number in history
                {
                    char* histstr = newstr();
                    sprintf(histstr, "%i", total_history + 1);
                    strcat(workBuf, histstr);
                    break;
                }
            case '^':  // previous command in history
                {
                    if (line_history[0] != NULL && strlen(line_history[0]))
                    {
                        char histstr[strlen(&line_history[0][0])];
                        sprintf(histstr, "%s", &line_history[0][0]);
                        strcat(workBuf, histstr);
                    }
                    else
                    {
                        strcat(workBuf, "(no history)");
                    }
                    break;
                }
            case 'd':  // current working directory, truncated
                if (strcmp(current_directory, current_user_home_dir) == 0)
                {
                    strcat(workBuf, "~");
                }
                else
                {
                    char* dir = newstr();
                    if (strtrnc('/', current_directory, dir, cwd_truncate_to))
                    {
                        strcat(workBuf, current_directory);
                    }
                    else
                    {
                        strcat(workBuf, dir);
                    }
                    free(dir);
                }
                break;
            case 'D':  // current working directory, full
                strcat(workBuf, current_directory);
                break;
            default:  // not recognised, treat it as an escaped character (e.g. for printing a % which isn't the dynamic one)
                strncat(workBuf, &c, 1);
                break;
            }
        }
        // run a command and put it's stdout result in
        else if (c == '`')
        {
            c = format[++i];
            char* com = newstr();
            while (c != '`')
            {
                strncat(com, &c, 1);
                c = format[++i];
            }
            const char* res = system_ol(com, 0, 1);
            strcat(workBuf, res);
            free(com);
        }
        // add the final %/# dynamically
        else if (c == '%' || c == '#')
        {
            if (current_user_id == 0)
                strcat(workBuf, "#");
            else
                strcat(workBuf, "%");
        }
        // normal character, just add it
        else
        {
            strncat(workBuf, &c, 1);
        }
    }

    // always include a trailing space for readability
    if (workBuf[strlen(workBuf) - 1] != ' ')
    {
        strcat(workBuf, " ");
    }

    return;
#endif
}

int tset(char* argstr)
{
    //    printf("%s\r\n", argstr);

    // take a copy of the string
    char* str = malloc(strlen(argstr));
    strcpy(str, argstr);

    int argc;
    char* argv[2];
    if (!strsp('=', str, &argc, argv, 2))
    {
        // ignore a preceding dollar
        // if (argv[0][0] == '$')
        //     argv[0]++;

        if (argc >= 2)
        {
            if (strcmp(argv[0], "PS1") == 0 || strcmp(argv[0], "prompt") == 0)
            {
                // free(PS1);
                strcpy(PS1, argv[1]);
                lsh_prompt_string(1, PS1);
                return 0;
            }
            else if (strcmp(argv[0], "TERM") == 0 || strcmp(argv[0], "term") == 0)
            {
                // free(PS1);
                strcpy(TERM, argv[1]);
                lsh_prompt_string(1, TERM);
                return 0;
            }
        }
    }
    return -1;
}

void prn(int n)
{
    char c[11];
    char* p;
    char sign;

    if (n < 0)
    {
        n = -n;
        sign = '-';
    }
    else
    {
        sign = '\0';
    }

    p = c;
    while (n > 0)
    {
        *p++ = n % 10 + '0';
        n /= 10;
    }

    if (sign != '\0')
        putc(sign, stdout);

    while (p >= c)
    {
        putc(*--p, stdout);
    }
}

void err(char* s)
{
    puts(s);
    //    puts("\n");
    if (!prompt)
    {
        lseek(STDIN, 0, SEEK_END);
        exit(-1);
    }
}

int any(int c, char* s)
{
    while (*s)
    {
        if (c == *s++)
            return 1;
    }
    return 0;
}

int equal(char* s1, char* s2)
{
    while (*s1 == *s2++)
    {
        if (*s1++ == '\0')
            return 1;
    }
    return 0;
}

void scan(unsigned long* at, char (*f)(char))
{
    char *p, c;
    unsigned long* t;

    t = at + DCOM;
    while ((p = (char*)*t++) != 0)
        while ((c = *p) != '\0')
            *p++ = (*f)(c);
}

char tglob(char c)
{
    if (any(c, "[?*"))
        glob = 1;
    return c;
}

char trim(char c)
{
    return (c & 0x7f);
}

unsigned long* tree(int n)
{
    unsigned long* t;

    t = treep;
    treep += n;
    if (treep > treeend)
    {
        puts("Command line overflow\n");
        error++;
        // reset();
        exit(-1);
    }
    return t;
}

unsigned long* parse1(char** p1, char** p2);
unsigned long* parse2(char** p1, char** p2);
unsigned long* parse3(char** p1, char** p2);

unsigned long* parse(char** p1, char** p2)
{
    while (p1 != p2)
    {
        if (any(**p1, ";&\n"))
        {
            p1++;
        }
        else
        {
            return parse1(p1, p2);
        }
    }
    return NULL;
}

/* Stage 1 parses command list whose left and right trees store its subcommand,
 * while left tree to be passed down to the next stage and right tree to be recursed.
 */
unsigned long* parse1(char** p1, char** p2)
{
    char **p, c;
    unsigned long *t, *t1;
    int l;

    l = 0;
    for (p = p1; p != p2; p++)
    {
        switch (**p)
        {
        case '(':
            l++;
            continue;
        case ')':
            if (--l < 0)
                error++;
            continue;
        case '&':
        case ';':
        case '\n':
            // Parathesis should be passed down to the next stage.
            if (l == 0)
            {
                c = **p;
                t = tree(4);
                t[DTYP] = TLST;
                t[DLEF] = (unsigned long)parse2(p1, p);
                t[DFLG] = 0;
                // Push down the attribute immediately.
                if (c == '&')
                {
                    t1 = (unsigned long*)t[DLEF];
                    t1[DFLG] = FAND | FPRS | FINT;
                }
                t[DRIT] = (unsigned long)parse(p + 1, p2);
            }
            return t;
        }
    }

    // Not found, transfer into the next stage.
    // Note: we should check whether parathesis is complete or not.
    if (l == 0)
    {
        return parse2(p1, p2);
    }
    else
    {
        error++;
        return NULL;
    }
}

/* Stage 2 parses filter whose left tree to be transferred to stage 3
 * and right tree to be recursed.
 */
unsigned long* parse2(char** p1, char** p2)
{
    char** p;
    unsigned long* t;
    int l;

    l = 0;
    for (p = p1; p != p2; p++)
    {
        switch (**p)
        {
        case '(':
            l++;
            continue;
        case ')':
            if (--l < 0)
                error++;
            continue;
        case '|':
        case '^':
            // Here we pass parathesis down to the next stage.
            if (l == 0)
            {
                t = tree(4);
                t[DTYP] = TFIL;
                t[DLEF] = (unsigned long)parse3(p1, p);
                t[DRIT] = (unsigned long)parse2(p + 1, p2);
                t[DFLG] = 0;  // Note: Attribute to be pushed down in execute() function.
                return t;
            }
        }
    }

    /* Here we do not need to check whether parathesis is complete (l == 0)
     * for we are in the middle stage.
     */
    return parse3(p1, p2);
}

/* Stage 3 parses parathesis command which should be transferred to stage 1
 * and simple command which to be generated directly.
 */
unsigned long* parse3(char** p1, char** p2)
{
    char **p, c;
    char **lp, **rp;
    unsigned long* t;
    int n, l;
    unsigned long input, output, flag;

    flag = 0;
    // Last sub command in parathesis commands.
    if (**p2 == ')')
    {
        flag |= FPAR;
    }

    lp = NULL;
    rp = NULL;
    input = 0;
    output = 0;
    n = 0;
    l = 0;

    for (p = p1; p != p2; p++)
    {
        switch (c = **p)
        {
        case '(':
            if (l == 0)
            {
                if (lp != NULL)
                    error++;
                lp = p + 1;
            }
            l++;
            continue;

        case ')':
            if (--l == 0)
                rp = p;
            continue;

        case '>':
            p++;
            if (p != p2 && **p == '>')
            {
                flag |= FCAT;
            }
            else
            {
                p--;
            }
            // Note: Here's no break!
        case '<':
            if (l == 0)
            {
                p++;
                // No character
                if (p == p2)
                {
                    error++;
                    p--;
                }
                // Illegal character
                if (any(**p, "<>("))
                {
                    error++;
                }
                // Input&output redirection
                if (c == '<')
                {
                    if (input != 0)
                        error++;
                    input = (unsigned long)*p;
                }
                else
                {
                    if (output != 0)
                    {
                        error++;
                    }
                    output = (unsigned long)*p;
                }
            }
            continue;

        default:
            // Simple command, store command arguments
            if (l == 0)
            {
                p1[n++] = *p;
            }
        }
    }

    // Parathesis command
    if (lp != 0)
    {
        if (n != 0)
            error++;
        t = tree(5);
        t[DTYP] = TPAR;
        t[DSPR] = (unsigned long)parse1(lp, rp);
        goto OUT;
    }

    // Simple command
    if (n == 0)
        error++;
    p1[n++] = NULL;
    t = tree(n + DCOM);
    t[DTYP] = TCOM;
    for (l = 0; l < n; l++)
        t[l + DCOM] = (unsigned long)p1[l];
    t[l + DCOM] = 0;  // pointer to NULL
OUT:
    t[DFLG] = flag;
    t[DLEF] = input;
    t[DRIT] = output;
    return t;
}

void texec(char* path, unsigned long* t)
{
    extern int errno;

    execv(path, (char**)(t + DCOM));

    if (errno == ENOEXEC)
    {
        if (*linep != '\0')
            t[DCOM] = (unsigned long)linep;
        t[DSPR] = (unsigned long)"/bin/sh";
        execv((char*)t[DSPR], (char**)(t + DSPR));
        puts("No shell!\n");
        exit(-1);
    }

    if (errno == ENOMEM)
    {
        puts((char*)t[DCOM]);
        err(": too large");
        exit(-1);
    }
}

void pwait(int p, unsigned long* t)
{
    int pid, error, status;

    if (p == 0)
        return;

    for (;;)
    {
        pid = wait(&status);
        if (pid == -1)
            break;
        error = status & 0x7f;
        if (mesg[error])
        {
            if (pid != p)
            {
                prn(pid);
                puts(": ");
            }
            puts(mesg[error]);
            if (status & 0x80)
                puts(" -- Core dumped\n");
        }

        if (error)
            err("");

        if (pid == p)
            break;
    }
}

void addhistory(const char* message)
{
    if (message != NULL && strlen(message) > 0 && (strcmp(line_history[0], message) != 0))
    {
        for (int j = HISTSIZ - 1; j > 0; j--)
        {
            strncpy(line_history[j], line_history[j - 1], COMMANSIZ);
        }
        strncpy(line_history[0], message, COMMANSIZ);
        hist_index++;
        total_history++;

        if (hist_index >= HISTSIZ)
            hist_index = HISTSIZ - 1;
    }
    return;
}

void execute(unsigned long* t, int* pf1, int* pf2)
{
    unsigned long flag;
    unsigned long* t1;
    char *cp1, *cp2;
    int pid, fd, pv[2];
    extern int errno;

    if (t == NULL)
        return;

    switch (t[DTYP])
    {
    case TCOM:
        cp1 = (char*)t[DCOM];

        addhistory(cp1);

        /* print working directory */
        if (equal(cp1, "pwd"))
        {
            getcwd(current_directory, DIRSIZ);
            fprintf(stdout, "%s\n", current_directory);
            return;
        }

        /* print history */
        if (equal(cp1, "history"))
        {
            for (int i = HISTSIZ - 1, j = 0; i >= 0; i--, j++)
            {
                if (strlen(line_history[i]))
                {
                    printf("%2d. %s\r\n", (total_history - i), line_history[i]);
                }
            }
            return;
        }

        /* Change directory */
        if (equal(cp1, "cd") || equal(cp1, "chdir"))
        {
            if (t[DCOM + 1])
            {
                if (chdir((char*)t[DCOM + 1]) != 0)
                    perror("cd");
                else
                {
                    lsh_prompt_string(true, PS1);
                }
            }
            else
            {
                err("cd: arg count");
            }
            return;
        }

        /* set terminal values */
        if (equal(cp1, "export") || equal(cp1, "set"))
        {
            if (t[DCOM + 1])
            {
                if (tset((char*)t[DCOM + 1]) != 0)
                {
                    err("set: could not set");
                }
            }
            else
            {
                err("set: arg count");
            }
            return;
        }

        if (equal(cp1, "shift"))
        {
            if (dolc < 1)
            {
                puts("shift: arg count");
                return;
            }
            dolv[1] = dolv[0];
            dolv++;
            dolc--;
            return;
        }

        /* exit shell */
        if (equal(cp1, "exit"))
        {
            if (prompt)
            {
                exit(0);
            }
            prn("exit: cannot exit\n");
            return;
        }

        /* login a user */
        if (equal(cp1, "login"))
        {
            if (prompt)
            {
                execv("/bin/login", (char**)(t + DCOM));
            }
            puts("login: cannot execte");
            return;
        }

        if (equal(cp1, "newgrp"))
        {
            if (prompt)
            {
                execv("/bin/newgrp", (char**)(t + DCOM));
            }
            puts("newgrp: cannot execte");
            return;
        }

        if (equal(cp1, "wait"))
        {
            pwait(-1, 0);
            return;
        }

        if (equal(cp1, ":"))
            return;

    // Note: Here's no break! self-defined command below
    case TPAR:
        flag = t[DFLG];
        pid = 0;
        if (!(flag & FPAR))
            pid = fork();
        if (pid == -1)
        {
            err("try again");
            return;
        }

        // Parent process (shell): NON-FPAR will run here
        if (pid != 0)
        {
            // FPIN will close pipe descriptor pv[2].
            if (flag & FPIN)
            {
                close(pf1[0]);
                close(pf1[1]);
            }
            // Print PID
            if (flag & FPRS)
            {
                prn(pid);
                puts("\n");
            }
            // No wait
            if (flag & FAND)
                return;
            // Note: we do not close FPOU's pv[2] here for it is to be reused by FPIN sequently.
            if (!(flag & FPOU))
                pwait(pid, t);

            return;
        }

        // Redirect STDIN
        if (t[DLEF])
        {
            fd = open((char*)t[DLEF], 0);
            if (fd < 0)
            {
                puts((char*)t[DLEF]);
                err(": cannot open");
                exit(-1);
            }
            // close(STDIN);
            dup2(fd, STDIN);
            close(fd);
        }

        // Redirect STDOUT
        if (t[DRIT])
        {
            if (flag & FCAT)
            {
                fd = open((char*)t[DRIT], 1);
                if (fd >= 0)
                {
                    lseek(fd, 0, SEEK_END);
                    goto f1;
                }
            }
            fd = creat((char*)t[DRIT], 0666);
            if (fd < 0)
            {
                puts((char*)t[DRIT]);
                err(": cannot creat");
                exit(-1);
            }
f1:
            // close(STDOUT);
            dup2(fd, STDOUT);
            close(fd);
        }

        // Redirect pipe in
        if (flag & FPIN)
        {
            // close(STDIN);
            dup2(pf1[0], STDIN);
            close(pf1[0]);
            close(pf1[1]);
        }

        // Redirect pipe out
        if (flag & FPOU)
        {
            // close(STDOUT);
            dup2(pf2[1], STDOUT);
            close(pf2[0]);
            close(pf2[1]);
        }

        // Ignore interrupt && no input && non pipe in, redirect STDIN to /dev/null
        // STDIN may be forked from parent.
        if ((flag & FINT) && !t[DLEF] && !(flag & FPIN))
        {
            fd = open("/dev/null", 0);
            // close(STDIN);
            dup2(fd, STDIN);
            close(fd);
        }

        if (!(flag & FINT) && setintr)
        {
            signal(SIGINT, SIG_DFL);
            signal(SIGQUIT, SIG_DFL);
        }

        // TPAR recursive, exit immediately
        if (t[DTYP] == TPAR)
        {
            // Push down attribute
            if ((t1 = (unsigned long*)t[DSPR]) != NULL)
                t1[DFLG] |= flag & FINT;
            execute(t1, NULL, NULL);
            exit(0);
        }

        glob = 0;
        scan(t, &tglob);
        if (glob)
        {
            t[DSPR] = (unsigned long)"glob";  //"/etc/glob";
            // execv((char*)t[DSPR], (char**)(t + DSPR));
            //  char** globarg = (char**)malloc(sizeof(char*) * 2);
            //  globarg[0] = "glob";
            //  globarg[1] = (char**)(t + DSPR);
            if (glob_main(3, (char**)(t + DSPR)))
            {  // converted to glob internally instead of externally
                puts("glob: cannot execute");
                exit(-1);
            }
        }

        scan(t, &trim);
        *linep = '\0';
        texec((char*)t[DCOM], t);
        cp1 = linep;
        cp2 = "/usr/bin/";
        while ((*cp1 = *cp2++) != '\0')
            cp1++;
        cp2 = (char*)t[DCOM];
        while ((*cp1++ = *cp2++) != '\0')
            continue;
        texec(linep + 4, t);
        texec(linep, t);
        fprintf(stderr, "%s", (char*)t[DCOM]);
        err(": not found");
        exit(-1);

    // Note: Here's no break!
    case TFIL:
        flag = t[DFLG];
        pipe(pv);
        // Push down filter attribute
        t1 = (unsigned long*)t[DLEF];
        t1[DFLG] |= FPOU | (flag & (FPIN | FINT | FPRS));
        execute(t1, pf1, pv);
        t1 = (unsigned long*)t[DRIT];
        t1[DFLG] |= FPIN | (flag & (FPOU | FINT | FAND | FPRS));
        execute(t1, pv, pf2);
        return;

    case TLST:
        // Push down list attribute.
        flag = t[DFLG] | FINT;
        if ((t1 = (unsigned long*)t[DLEF]) != NULL)
            t1[DFLG] |= flag;
        execute(t1, NULL, NULL);
        if ((t1 = (unsigned long*)t[DRIT]) != NULL)
            t1[DFLG] |= flag;
        execute(t1, NULL, NULL);
        return;
    }
}

int readc()
{
    int cc;
    char c;

    // Option -c
    if (arginp)
    {
        if (arginp == (void*)1)
            exit(0);
        if ((c = *arginp++) == '\0')
        {
            c = '\n';
            arginp = (void*)1;
        }
        return (int)c;
    }

    // Option -t
    if (onelflg == 1)
        exit(0);
    if (read(STDIN, &cc, 1) != 1)
        exit(-1);
    if (cc == '#')
        while (cc != '\n')
            if (read(STDIN, &cc, 1) != 1)
                exit(-1);
    if (cc == '\n' && onelflg)
        onelflg--;

    return cc;
}

char getch()
{
    char c;

    if (peekc)
    {
        c = peekc;
        peekc = 0;
        return c;
    }

    if (tokp > etokp)
    {
        tokp -= 10;
        while ((c = getch()) != '\n')
            continue;
        tokp += 10;
        err("Too many toks");
        overflow++;
        return c;
    }

    if (linep > elinep)
    {
        linep -= 10;
        while ((c = getch()) != '\n')
            continue;
        linep += 10;
        err("Too many characters");
        overflow++;
        return c;
    }

GET:
    // '$'
    if (dolp)
    {
        c = *dolp++;
        if (c != '\0')
            return c;
        dolp = 0;
    }

    c = readc();

    // '\'
    if (c == '\\')
    {
        c = readc();
        if (c == '\n')
            return ' ';
        return (c | QUOTE);
    }

    if (c == '$')
    {
        c = readc();
        // '$n'
        if (c >= '0' || c <= '9')
        {
            if (c - '0' < dolc)
                dolp = dolv[c - '0'];
            goto GET;
        }
        // '$$'
        if (c == '$')
        {
            dolp = pidp;
            goto GET;
        }
    }

    return (c & 0x7f);
}

void token()
{
    char c, c1;

    *tokp++ = linep;

TOKEN:
    c = getch();
    // fputc(c, stdout);
    switch (c)
    {
    case '\t':
    case ' ':
        goto TOKEN;

    case '\'':
    case '"':
        c1 = c;
        // Note: Here we should read directly from input stream!
        while ((c = readc()) != c1)
        {
            if (c == '\n')
            {
                error++;
                peekc = c;  // '\n' should be pushed back for next session
                return;
            }
            *linep++ = c | QUOTE;
        }
        goto SEPERATE;

    case '&':
    case ';':
    case '<':
    case '>':
    case '(':
    case ')':
    case '|':
    case '^':
    case '\n':
        *linep++ = c;
        *linep++ = '\0';
        return;
    }

    // Push back non meta character
    peekc = c;

SEPERATE:
    for (;;)
    {
        c = getch();
        // Here is token seperator.
        if (any(c, " '\"\t;&<>()|^\n"))
        {
            peekc = c;  // Push back as next token
            if (any(c, "\"'"))
                goto TOKEN;
            *linep++ = '\0';
            return;
        }
        *linep++ = c;
    }
}

void session()
{
    char* cp;
    unsigned long* t;

    tokp = toks;
    etokp = toks + TOKSIZ - 5;
    linep = line;
    elinep = line + LINSIZ - 5;
    error = 0;
    overflow = 0;

    // End of one session when the first character of line buffer is '\n'
    do
    {
        cp = linep;
        token();
    } while (*cp != '\n');

    treep = trebuf;
    treeend = (unsigned long*)trebuf + TRESIZ;

    if (!overflow)
    {
        if (error == 0)
        {
            // setexit();
            // if (error)
            // return;
            t = parse(toks, tokp);
        }

        if (error)
        {
            err("Syntax error!");
        }
        else
        {
            execute(t, NULL, NULL);
        }
    }
}

int _tsh(int argc, char** argv)  // previously main
{
    int i, pid;

    for (i = STDERR; i < 16; i++)
        close(i);
    dup2(STDOUT, STDERR);
    for (i = 4; i >= 0; i--)
    {
        pidp[i] = pid % 10 + '0';
        pid /= 10;
    }

    // allocate all the possible space we may ever need for the prompt
    if (prompt == NULL)
        prompt = malloc(PROMPSIZ * sizeof(char));

    // likewise for the host name
    if (current_host_name == NULL)
        current_host_name = malloc(DIRSIZ * sizeof(char));

    // likewise for the current directory
    if (current_directory == NULL)
        current_directory = malloc(DIRSIZ * sizeof(char));

    // Create the prompt string
    lsh_prompt_string(true, PS1);

    if (argc > 1)
    {
        prompt = NULL;
        if (*argv[1] == '-')
        {
            **argv = '-';
            if (argv[1][1] == 'c' && argc > 2)
                arginp = argv[2];
            else if (argv[1][1] == 't')
                onelflg = 2;
        }
        else
        {
            i = open(argv[1], 0);
            if (i < 0)
            {
                fputs(argv[1], stderr);
                err(": cannot open");
            }
            // close(STDIN);
            dup2(i, STDIN);
            close(i);
            signal(SIGINT, SIG_DFL);
            signal(SIGQUIT, SIG_DFL);
        }
    }

    if (**argv == '-')
    {
        setintr++;
        signal(SIGINT, SIG_IGN);
        signal(SIGQUIT, SIG_IGN);
    }

    dolv = argv + 1;
    dolc = argc - 1;

    for (;;)
    {
        // If there is a prompt, print it.
        if (prompt != NULL)
        {
            // for (int i = 0; i < strlen(prompt); i++)
            //{
            // fputc(prompt[i], stdout);
            fputs(prompt, stdout);
            fflush(stdout);
            //}
        }
        peekc = getch();  // Pre-read one character
        session();

        // if (onelflg == 0)
        lsh_prompt_string(true, PS1);
    }

    return 0;
}

// Main entry point to the shell
int main(int argc, char** argv)
{
    PS1 = newstr();
    strcat(PS1, default_prompt);

    TERM = newstr();
    strcat(TERM, default_term);

    int use_inract,
      show_vers, use_verbs, do_logins, is_sh, quiet;
    char* arg;

    SECURE_IN = stdin;
    SECURE_OUT = stderr;

    // int i, pid;

    // for (i = STDERR; i < 16; i++)
    //     close(i);
    // dup2(STDOUT, STDERR);
    // for (i = 4; i >= 0; i--)
    // {
    //     pidp[i] = pid % 10 + '0';
    //     pid /= 10;
    // }

    int argc_o = argc;
    char** argv_o = argv;

    if (strcmp("sh", argv[0]) == 0)
    {
        is_sh++;
    }

    argv++;

    for (argc--; (argc > 0); argc--)
    {
        arg = *argv;

        if (*arg == '+' || *arg == '-')
        {
            arg++;
            argv++;
            switch (*arg)
            {
                // Version
            case 'v':
                {
                    fprintf(stderr, "\r\n%s, version %s %s\r\n", ProductInfo, BuildNumber, BuildDate);
                    fprintf(stderr, "%s\r\n", CopyrightInfo);
                    fprintf(stderr, "\r\n%s\r\n", LicenseInfo);
                    fprintf(stderr, "%s\r\n", FreeSoftwareDisclaimer);
                    exit(0);
                    exit(0);
                    return 0;
                }
                // Interactive - does this anyway as long as it isn't a file or command...
            case 'i':
                {
                    use_inract++;
                    break;
                }
            case 'c':
                {
                    // if called in command mode, we're just going to launch that shell instead and pass our arguments through
                    int ret = _tsh(argc_o, argv_o);
                    exit(ret);
                    return ret;
                }
            case 't':
                {
                    // if called in thompson mode, we're just going to launch that shell instead and pass our arguments through
                    int ret = _tsh(argc_o, argv_o);
                    exit(ret);
                    return ret;
                }
                // Login -- don't work yet
            case 'l':
                {
                    do_logins++;
                    break;
                }
            default:
                {
                    fprintf(stderr, "Bad flag: %c\r\n", *arg);
                    exit(1);
                    return 1;
                }
            }
        }
    }

    // grab user info
    current_user = getpwuid(geteuid());

    // // if we're logging in then we need to execute the login scripts, e.g. /etc/profile
    // if (do_logins)
    // {
    //     if (access("/etc/profile", F_OK) == 0)
    //         execl(argv_o[0], "lsh", "-t", "/etc/profile", (char*)NULL);
    //     if (access("~/.login", F_OK) == 0)
    //         execl(argv_o[0], "lsh", "-t", "~/.login", (char*)NULL);
    //     if (access("~/.profile", F_OK) == 0)
    //         execl(argv_o[0], "lsh", "-t", "~/profile", (char*)NULL);
    // }

    // //Always run the init scripts
    // if (access("/etc/.lshrc", F_OK) == 0)
    //     system("./a.out /etc/.lshrc");
    // if (access("~/.lshrc", F_OK) == 0)
    //     system("./a.out ~/.lshrc");

    signal(SIGINT, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);

    int ret = _tsh(argc_o, argv_o);
    exit(ret);
    return ret;

    // //create prompt string
    // lsh_prompt_string(freshPromptNeeded);

    // // if we're interactive (or drop back to interractive), show the prompt and wait for text
    // if (use_inract)
    // {
    //     while (true)
    //     {
    //         //calculate the latest login string
    //         lsh_prompt_string(freshPromptNeeded);
    //         //const char* message = lsh_read_line(prompt, freshPromptNeeded);  //this loads char by char, and only returns if a complete line is entered
    //         //try and process that message
    //         //if (message != NULL)
    //         //{
    //         freshPromptNeeded = true;
    //         // lsh_add_command_to_history(message);
    //         printf("\r\n");  //Print the new line missing from the echo.
    //                          // int response = lsh_command_handler(message);
    //         freshPromptNeeded = true;
    //         //}
    //         //else
    //         //    freshPromptNeeded = false;
    //     }
    // }

    return 0;
}
