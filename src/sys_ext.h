#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/**
 * @brief   Creates a pointer to a new blank string, pre-initialised to 0-length with a trailing zero
 * @return  char*
 */
char* newstr()
{
    char* res = (char*)malloc(sizeof(char) * 1);
    res[0] = 0;
    return res;
}

/**
 * @brief   Split string by character token
 * @param   sp token to split by
 * @param   str string to split
 * @param   argc number of resulting substrings
 * @param   argv list of the resulting substrings
 * @param   len length of the allowed list of resulting substrings
 * @return  -1 for error, 0 for no error.
 */
int strsp(char sp, char* str, int* argc, char** argv, int len)
{
    int lib, idx;
    /* Used for checking if the line is blank */
    lib = 1;

    /* Clear the number of arguments collected */
    *argc = 0;

    /* Clear the argv array */
    for (idx = 0; idx < len; idx++)
    {
        argv[idx] = newstr();
    }

    /* cut off blank spaces from the end of the string (just change to 0) */
    for (idx = strlen(str) - 1; idx >= 0; idx--)
    {
        if (isblank(str[idx]))
            str[idx] = 0;
        else
        {
            /* If it's not a blank character, then the line has data and we
             * break */
            lib = 0;
            break;
        }
    }

    /* If the line is blank, then there are no arguments */
    if (lib)
    {
        return -1;
    }
    else
    {
        /* Go through the entered line and split it by string into an argv array
         */
        for (idx = 0; (idx < strlen(str)) && (*argc) < len; idx++)
        {
            /* Replace tabs with token */
            if (str[idx] == '\t')
            {
                str[idx] = sp;
            }

            /* if the previous character was a space, ignore this one and don't
             * add it to args */
            if (idx > 0 && str[idx] == sp && str[idx - 1] == sp)
            {
            }
            /* If we've hit a separator, move ontothe next entry (ignore if
             * we're still on the first character)*/
            else if (idx > 0 && str[idx] == sp)
            {
                (*argc)++;
                if ((*argc) >= len)
                    break;
            }
            /* If it's a printable character add it to the current argument */
            else if (isprint(str[idx]))
            {
                strncat(argv[(*argc)], &str[idx], 1);
            }
        }

        /* If the line has non-space characters, we must have at least one arg,
         * so add it */
        if ((*argc) < len)
        {
            (*argc)++; /* always at least 1 arg */
        }
    }
    return 0;
}

/**
 * @brief   truncate a string with splitting characters to the number of split sections
 * @param   sp   token to split by
 * @param   str  string to
 * @param   len  number of splits to take off the end in the truncate
 * @param   res  resulting char* string (should be empty but not null)
 * @return  -1 for error, 0 for success
 */
int strtrnc(char sp, char* str, char* res, int len)
{
    int argc;
    char* argv[20];
    if (!strsp(sp, str, &argc, argv, 20))
    {
        int st = 0;
        if (argc - len > 0)
            st = argc - len;
        else
            st = 0;

        for (int i = st; i < argc; i++)
        {
            strcat(res, argv[i]);
            strncat(res, &sp, 1);
        }
        res[strlen(res) - 1] = 0;
        return 0;
    }
    return -1;
}

/**
 * @brief   Execute a command and get the result of stdout and optionally stderr up to the end of the first line (excl \r\n)
 * @param   cmd The system command to run.
 * @param   STDERR Whether to include the stderr stream as well as stdout
 * @param   SL Whether to only grab up to the first new line
 * @return  The string command line output of the command, or an empty (but not NULL) string if fail
 */
const char* system_ol(const char* cmd, int STDERR, int SL)
{
    char* data = (char*)malloc(sizeof(char) * 1);  // allocate some space for it

    // take a copy of the command string for any manipulation
    char* commedit = (char*)malloc((strlen(cmd) + 1) * sizeof(char));
    strcpy(commedit, cmd);

    FILE* stream;
    char buffer[256];

    // append the redirect from stderr to stdout if we need that
    if (STDERR)
        commedit = strcat(commedit, " 2>&1");

    // clear anything we had left in the stdout buffer from the parent program
    fflush(stdout);

    // pipe+open (i.e. connect to the stdout to grab info when we run it)
    stream = popen(commedit, "r");

    // if steam opened, then we can capture the data
    if (stream)
    {
        // while data is still being written, capture it.
        while (1)
        {
            if (feof(stream))  // if we reached the end of the stream
            {
                break;
            }
            if (ferror(stream))  // if we had an error
            {
                break;
            }
            if (fgets(buffer, 256, stream) != NULL)  // if we can read this
            {
                // cut the end off the string if requested
                if (SL)
                {
                    for (int i = strlen(buffer) - 1; i >= 0; i++)
                    {
                        if (buffer[i] == '\t' || buffer[i] == '\n' || buffer[i] == '\r' || buffer[i] == ' ')
                        {
                            buffer[i] = 0;
                        }
                        else
                            break;
                    }
                }

                data = strcat(data, buffer);
            }
        }
        // pclose(stream);
    }
    return data;
}

/**
 * @brief   Execute a command and get the result of stdout and optionally stderr
 * @param   cmd The system command to run.
 * @param   STDERR Whether to include the stderr stream as well as stdout
 * @return  The string command line output of the command, or an empty (but not NULL) string if fail
 */
const char* system_o(const char* cmd, int STDERR)
{
    return system_ol(cmd, STDERR, 0);

    char* data = (char*)malloc(sizeof(char));  // allocate some space for it

    // take a copy of the command string for any manipulation
    char* commedit = (char*)malloc((strlen(cmd) + 1) * sizeof(char));
    strcpy(commedit, cmd);

    FILE* stream;
    char buffer[256];

    // append the redirect from stderr to stdout if we need that
    if (STDERR)
        commedit = strcat(commedit, " 2>&1");

    // clear anything we had left in the stdout buffer from the parent program
    fflush(stdout);

    // pipe+open (i.e. connect to the stdout to grab info when we run it)
    stream = popen(commedit, "r");

    // if steam opened, then we can capture the data
    if (stream)
    {
        // while data is still being written, capture it.
        while (1)
        {
            if (feof(stream))  // if we reached the end of the stream
            {
                break;
            }
            if (ferror(stream))  // if we had an error
            {
                break;
            }
            if (fgets(buffer, 256, stream) != NULL)  // if we can read this
            {
                data = strcat(data, buffer);
            }
        }
        // pclose(stream);
    }
    return data;
}
