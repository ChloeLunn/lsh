#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void glob_toolong()
{
    write(STDOUT, "Arg list too long\n", 18);
    exit(-1);
}

char* glob_cat(char* as1, char* as2)
{
    char *s1, *s2;
    char c;

    s2 = string;
    s1 = as1;
    while ((c = *s1++) != '\0')
    {
        if (s2 > ab + STRSIZ)
            glob_toolong();
        c &= 0x7f;
        if (c == '\0')
        {
            *s2++ = '/';  // Recover '/'
            break;
        }
        *s2++ = c;
    }

    s1 = as2;
    do
    {
        if (s2 > ab + STRSIZ)
            glob_toolong();
        *s2++ = c = *s1++;
    } while (c != '\0');

    s1 = string;
    string = s2;
    return s1;
}

int glob_compar(const char* s1, const char* s2)
{
    int ret;
    while ((ret = *s1 - *s2++) == 0 && *s1++ != '\0')
        continue;
    return ret;
}

// Bubble-glob_sort
void glob_sort(char** oav)
{
    char **p1, **p2;
    char** xchg;
    char* tmp;

    p2 = av;
    do
    {
        xchg = NULL;
        for (p1 = oav + 1; p1 < p2; p1++)
        {
            if (glob_compar(*(p1 - 1), *p1) > 0)
            {
                xchg = p1;
                tmp = *p1;
                *p1 = *(p1 - 1);
                *(p1 - 1) = tmp;
            }
        }
        p2 = xchg;
    } while (xchg != NULL);
}

void glob_execute(char* file, char** args)
{
    execv(file, args);
    if (errno == ENOEXEC)
    {
        args[0] = file;
        *--args = "/bin/sh";  // ava[0]
        execv(*args, args);   // re-exec /bin/sh
    }
    if (errno == E2BIG)
        glob_toolong();
}

int glob_match1(char* str, char* pat)
{
    char s, p, rp, lp;
    int ok;
    int glob_match2(char*, char*);

    if ((s = *str++) != '\0')
        if (!(s & 0x7f))
            s = 0x80;

    switch (p = *pat++)
    {
    case '[':
        lp = 0xff;
        while ((rp = *pat++) != '\0')
        {
            if (rp == ']')
            {
                if (ok)
                    return glob_match1(str, pat);
                else
                    return 0;
            }
            else if (rp == '-')
            {
                if (s >= lp && s <= *pat++)
                    ok++;
            }
            else
            {
                if (s == (lp = rp))
                    ok++;  // at least glob_match one char will be ok
            }
        }
        return 0;  // missing ']'

    default:
        if (s != p)  // not glob_match
            return 0;

    case '?':
        if (s != '\0')
            return glob_match1(str, pat);
        return 0;  // Yet pattern not finished

    case '*':
        return glob_match2(--str, pat);

    case '\0':
        return (s == '\0');  // both finished
    }

    return 0;
}

int glob_match2(char* str, char* pat)
{
    if (*pat == '\0')  // '*' is the last char
        return 1;
    while (*str != '\0')
    {
        if (glob_match1(str++, pat))  // '*' glob_match finished when pattern glob_matched
            return 1;
    }
    return 0;  // Yet remain pattern char not glob_matched
}

int glob_match(char* str, char* pat)
{
    int glob_match1(char*, char*);

    // Omit hidden files
    if (*str == '.' && *pat != '.')
        return 0;
    return glob_match1(str, pat);
}

void glob_expand(char* as)
{
    char *s, *cs;
    DIR* dir;
    char** oav;
    struct dirent* direp;

    cs = as;
    s = cs;
    while (*cs != '*' && *cs != '?' && *cs != '[')
    {
        if (*cs++ == '\0')
        {
            *av++ = glob_cat(s, "");
            return;
        }
    }

    for (;;)
    {
        // There's no '/' character
        if (cs == s)
        {
            dir = opendir(".");
            s = "";
            break;
        }

        if (*--cs == '/')
        {
            *cs = '\0';
            dir = opendir(s == cs ? "/" : s);
            *cs++ = 0x80;  // Indiglob_cator
            break;
        }
    }

    if (dir < 0)
    {
        write(STDOUT, "No directory\n", 13);
        exit(-1);
    }

    oav = av;
    while ((direp = readdir(dir)) != NULL)
    {
        if (glob_match(direp->d_name, cs))
        {
            *av++ = glob_cat(s, direp->d_name);
            ncoll++;
        }
    }
    closedir(dir);
    glob_sort(oav);
}

int glob_main(int argc, char** argv)
{
    char* cp;

    string = ab;
    av = &ava[1];  // ava[0] is for "/bin/sh"

    if (argc < 3)
    {
        write(STDOUT, "Arg count\n", 10);
        return 0;
    }

    argv++;
    *av++ = *argv;
    while (--argc >= 2)
        glob_expand(*++argv);

    if (ncoll == 0)
    {
        write(STDOUT, "No glob_match\n", 9);
        return 0;
    }

    glob_execute(ava[1], &ava[1]);
    cp = glob_cat("/usr/bin/", ava[1]);
    glob_execute(cp + 4, &ava[1]);
    glob_execute(cp, &ava[1]);
    write(STDOUT, "Command not found\n", 19);

    return 0;
}
