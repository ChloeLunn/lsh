//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                   HEADER FOR FIXED INFORMATION AND VERSION INFO                     //
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Copyright (c) 2020 - No rights reserved.
// Author: C Lunn

#pragma once
#ifndef revision_h_defined

const char* ProductInfo = "lsh simple shell";
const char* CopyrightInfo = "Copyright (c) 2019-2021 Chloe Lunn, No rights reserved";
const char* BuildDate = "2021-11-10";
const char* BuildNumber = "1.0.0";
const char* LicenseInfo = "License MIT: <https://opensource.org/licenses/MIT>";
const char* FreeSoftwareDisclaimer = "This is free software; you are free to change and redistribute it.\r\n"
                                     "There is NO WARRANTY, to the extent permitted by law.\r\n";

#endif
#define revision_h_defined
